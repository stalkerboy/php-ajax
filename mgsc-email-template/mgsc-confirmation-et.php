<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multi-Step-Form email Template</title>
</head>

<body style="margin: 0;">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="max-width:768px;margin:0 auto;">
	<tr>
    <td align="center" valign="top" bgcolor="#fff">	
	
	<table style="width:100%;">
		<tr>
			<td style="padding:0 30px; background-color:#eeeeee;">				
								
				
<table style="width:100%;">
<tr>
<td align="left" style="padding:50px 0 0;">
<p><strong>Hi [mgs-sender-name],</strong></p>
<p>Thank you for getting in touch!</p>
<p>We appreciate you contacting us. One of our customer happiness members will be getting back to you shortly.</p>
<p>Thanks in advance for your patience.</p>
<p>Have a great day!</p>
</td>
</tr>
</table>

<table style="width:100%;">

</table>

<hr />
<table style="width:100%;">

</table>
				
							
			</td>
		</tr>
	</table>
		
	</td>
    </tr>
</table>

</body>
</html>
